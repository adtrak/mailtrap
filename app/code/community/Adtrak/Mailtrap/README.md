# Adtrak_Mailtrap

This module rewrites the standard Magento e-mail functionality to send all mail
to mailtrap.io. You will need to set some config before the module will work
correctly, which is done via System > Configuration.

The module works by rewriting the following models:

- Mage_Core_Model_Email
- Mage_Core_Model_Email_Template

If you have any modules which also rewrite the same models, you will experiance
conflicts (most probably this module will stop working, as it has a low sort
order when sorted alphaberically - i.e. adtrak starts with an a!).

Modules known to conflict are:

- Ebizmarts_MageMonkey (Adtrak_Mailtrap loses)
