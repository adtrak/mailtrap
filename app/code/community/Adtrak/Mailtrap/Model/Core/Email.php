<?php
/**
 * Adtrak mailtrap.io Integration
 *
 * @category  Adtrak
 * @package   Adtrak_Mailtrap
 * @copyright Copyright (c) 2013 Adtrak (http://www.adtrak.co.uk)
 * @license   http://www.adtrak.co.uk/license.txt
 */

/**
 * Rewrite to core/email to send to mailtrap.io
 *
 * @category Adtrak
 * @package  Adtrak_Mailtrap
 * @author   Mike Whitby <mike.whitby@adtrak.co.uk>
 */
class Adtrak_Mailtrap_Model_Core_Email extends Mage_Core_Model_Email
{
    /**
     * Rewrite of the send() function to re-route e-mail to mailtrap.io
     *
     * @return Adtrak_Mailtrap_Model_Core_Email
     */
    public function send()
    {
        /**
         * If we can't or don't want to use mailtrap, use the standard
         * send() functionality
         */
        if (!Mage::helper('adtrak_mailtrap')->useMailtrap()) {
            return parent::send();
        }

        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }

        $mail = new Zend_Mail();

        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        }
        else {
            $mail->setBodyText($this->getBody());
        }

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
            ->addTo($this->getToEmail(), $this->getToName())
            ->setSubject($this->getSubject());

        $mail->send(Mage::helper('adtrak_mailtrap/email')->getTransport());

        return $this;
    }
}
