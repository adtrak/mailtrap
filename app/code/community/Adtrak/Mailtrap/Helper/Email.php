<?php
/**
 * Adtrak mailtrap.io Integration
 *
 * @category  Adtrak
 * @package   Adtrak_Mailtrap
 * @copyright Copyright (c) 2013 Adtrak (http://www.adtrak.co.uk)
 * @license   http://www.adtrak.co.uk/license.txt
 */

/**
 * Adstrak mailtrap.io email helper
 *
 * @category Adtrak
 * @package  Adtrak_Mailtrap
 * @author   Mike Whitby <mike.whitby@adtrak.co.uk>
 */
class Adtrak_Mailtrap_Helper_Email extends Mage_Core_Model_Email
{
    /**
     * Returns a Zend mail transport with the mailtrap.io settings
     *
     * @return Zend_Mail_Transport_Smtp
     */
    public function getTransport()
    {
        return new Zend_Mail_Transport_Smtp(
            Mage::getStoreConfig('system/smtp/mailtrap_host'),
            array(
                'port'     => Mage::getStoreConfig('system/smtp/mailtrap_port'),
                'auth'     => 'login',
                'username' => Mage::getStoreConfig('system/smtp/mailtrap_username'),
                'password' => Mage::getStoreConfig('system/smtp/mailtrap_password'),
            )
        );
    }
}
