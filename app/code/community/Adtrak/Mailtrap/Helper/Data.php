<?php
/**
 * Adtrak mailtrap.io Integration
 *
 * @category  Adtrak
 * @package   Adtrak_Mailtrap
 * @copyright Copyright (c) 2013 Adtrak (http://www.adtrak.co.uk)
 * @license   http://www.adtrak.co.uk/license.txt
 */

/**
 * Adstrak mailtrap.io general helper
 *
 * @category Adtrak
 * @package  Adtrak_Mailtrap
 * @author   Mike Whitby <mike.whitby@adtrak.co.uk>
 */
class Adtrak_Mailtrap_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * If sending to mailtrap is disabled, or we can't due to a lack of settings
     * then return false, otherwise return true
     *
     * @return bool
     */
    public function useMailtrap()
    {
        $enabled = Mage::getStoreConfigFlag('system/smtp/mailtrap_enabled');
        $user    = Mage::getStoreConfig('system/smtp/mailtrap_username');
        $pass    = Mage::getStoreConfig('system/smtp/mailtrap_password');

        return ($enabled && $user && $pass);
    }
}
